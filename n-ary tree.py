class Node(object):

    def __init__(self, parent, data, level):
        self.children = []
        self.val = data
        self.level = level
        self.parent = parent

    def insert(self, parent, data, level):

        # if parent does not have a parent it is the root
        if parent is None:
            # create new node and assign it to the root
            child = Node(parent, data, level)

            # add child to the parent
            parent.children = []
            parent.children.append(child)

            # return the child (new parent node)
            return child

        # if the parents level is not one above the current level
        # elif parent.level != level-1:
        elif parent.level != level - 1:
            new_parent = parent
            child = Node(parent, data, level)

            # get parent till the level is the same
            while new_parent.level - child.level != -1:
                new_parent = new_parent.parent

            # assign the child the reference to the new parent
            child.parent = new_parent

            # assign the child to the new parent
            new_parent.children.append(child)
            return child

        else:
            # create new node and assign it to the parent
            child = Node(parent, data, level)

            # add child to the parent
            parent.children = []
            parent.children.append(child)

            # return the child (new parent node)
            return child


class Tree:

    def __init__(self):
        self.root = None

    def insert(self, parent, data, level):
        if self.root:  # check it the root exists
            return self.root.insert(parent, data, level)
        else:
            self.root = Node(parent, data, level)
            return self.root

    def pre_order(self, root):
        if root:
            # First print the data of node
            print(print_prefix(root)+""+root.val)

            # Then recur on left child
            for i in range(0, len(root.children), 1):
                self.pre_order(root.children[i])

    def find(self, root, node):
        global check
        # Then recur on left child
        for i in range(0, len(root.children), 1):
            self.find(root.children[i], node)

        if root.val == node.val and id(root) != id(node) and root.parent and root.parent.val == "JUMP":
            check = check or True
            return True

    def generate_code(self):

        # tree always starts with a statement
        self.munch_statement(self.root)

    def munch_statement(self, statement):
        global indentation, check
        # SEQ [[MOVE],statement]

        if statement.val == "SEQ":
            # if both children are move
            if statement.children[0].val == "MOVE" and statement.children[1].val == "MOVE":
                self.munch_move(statement.children[0])  # munch move left child
                self.munch_move(statement.children[1])  # munch move right child

            # if right is move
            elif len(statement.children) > 1 and statement.children[1].val == "MOVE":
                self.munch_statement(statement.children[0])  # munch statement left child
                self.munch_move(statement.children[1])      # munch move right child

            # if left is not move
            elif statement.children[0].val != "MOVE":
                self.munch_statement(statement.children[0])         # munch_statement left
                self.munch_statement(statement.children[1])         # munch_statement right

            else:
                self.munch_move(statement.children[0])              # munch left child move
                self.munch_statement(statement.children[1])         # munch right child statement

        # CALL[NAME[print], const]

        elif statement.val == "CALL" and statement.children[1].val == "CONST":

            if statement.children[0].val == "NAME":

                if statement.children[0].children[0].val == "print":
                    # print munched expression on the right side
                    print("\t"*indentation + "print ( " + self.munch_constant(statement.children[1]) + " )")

        # CALL[NAME[print], expr]
        elif statement.val == "CALL":

            if statement.children[0].val == "NAME":

                if statement.children[0].children[0].val == "print":

                    # print munched expression on the right side
                    print("\t"*indentation + "print ( " + self.munch_expr(statement.children[1]) + " )")

        # CJUMP [e1, e2, e3, e4, e5]

        elif statement.val == "CJUMP":

            if statement.parent.children[0].val == "LABEL":
                self.find(self.root, statement.parent.children[0].children[0])
                self.loop_check(statement)
                check = False
            elif statement.parent.children[0].children[1].val == "LABEL":

                check = False
                self.find(self.root, statement.parent.children[0].children[1].children[0])
                self.loop_check(statement)
                check = False

            else:
                self.loop_check(statement)

            # first check if the previous statement is a label can also be the right child

        # statement.children[0] == "JUMP"
        elif statement.val == "JUMP":

            # check if the label is the same as the label at the top of the stack for the while loop
            if stack and statement.children[0].val == stack[0]:
                stack.pop(0)
                indentation -= 1

            if statement.children[0].val == "done":
                indentation -= 1
                print("\t"*indentation + "else: ")
                indentation += 1

        # elif statement.val == "LABEL":
        #    if statement.val.children[0] == "done":
        #        indentation -= 1

    def munch_move(self, move):

        # MOVE[MEM[+[TEMP FP, CONST variable]], e]

        if move.children and move.children[0].val == "MEM" and move.children[0].children[0].val == "+":
            left_grandchild = move.children[0].children[0].children[0]
            right_grandchild = move.children[0].children[0].children[1]

            if left_grandchild.val == "TEMP" and right_grandchild.val == "CONST":

                # munch const on the right hand side

                if move.children[1].val == "CONST":
                    print("\t"*indentation + right_grandchild.children[0].val+" = " + self.munch_constant(move.children[1]))

                # else it is an expression
                else:
                    print("\t"*indentation + right_grandchild.children[0].val+" = " + self.munch_expr(move.children[1]))

            if left_grandchild.val == "CONST" and right_grandchild.val == "TEMP":

                # need to check if the right hand side is an expression or a constant

                # munch expr on the left hand side identified by the operator
                if move.children[1].val in operators:
                    print("\t"*indentation + left_grandchild.children[0].val+" = " + self.munch_expr(move.children[1]))

        # MOVE[MEM[+[CONST variable, TEMP FP]], e]

        elif move.children and move.children[0].val == "MEM" and move.children[0].children[0].val == "+":
            left_grandchild = move.children[0].children[0].children[0]
            right_grandchild = move.children[0].children[0].children[1]

            if left_grandchild.val == "CONST" and right_grandchild.val == "TEMP":
                print("\t"*indentation + "x" + self.munch_expr(move.children[1]))

        # MOVE[MEM[+[CONST variable, TEMP FP]], e]

        elif move.children and move.children[0].val == "MEM" and move.children[0].children[0].val == "+":
            left_grandchild = move.children[0].children[0].children[0]
            right_grandchild = move.children[0].children[0].children[1]

            if left_grandchild.val == "TEMP" and right_grandchild.val == "CONST":
                print("\t"*indentation + right_grandchild.children[0].val + self.munch_expr(move.children[1]))

    def munch_expr(self, expr):

        # CALL[NAME[input]]

        if expr.val == "CALL":

            if expr.children[0].val == "NAME":
                grand_child = expr.children[0].children[0]

                if grand_child.val == "input":
                    return "eval ( input () )"

        # we can have three cases
        # 1. op[e2, CONST[c]]
        # 2. op[CONST[C],e2]
        # 3. op[e1,e2]

        elif expr.val in operators:
            # self.munch_expr(expr.children[0])       # munch expression on the left side
            # self.munch_constant(expr.children[1])   # munch the constant on the right

            # +[e1,e2] -- check for expression on both sides
            if expr.children[0].val in operators and expr.children[1].val in operators:
                # return op[e2, e3]
                return self.munch_expr(expr.children[0]) + str(expr.val) + self.munch_expr(expr.children[1])

            elif expr.children[0].val == "CONST" and expr.children[1].val == "CONST":
                return self.munch_constant(expr.children[0]) + str(expr.val) + self.munch_constant(expr.children[1])

            # check for an expression on  the  left side and a constant on the other
            elif expr.children[0].val in operators and expr.children[1].val == "CONST":
                # return op[e2, e3]
                return self.munch_expr(expr.children[0]) + str(expr.val) + self.munch_constant(expr.children[1])

            # check for an expression on  the  left side and a constant on the other
            elif expr.children[0].val == "CONST" and expr.children[1].val in operators:
                # return op[e2, e3]
                return self.munch_constant(expr.children[0]) + str(expr.val) + self.munch_expr(expr.children[1])

            elif expr.children[0].val == "MEM" and expr.children[0].children[0].val == "+":
                left_grandchild = expr.children[0].children[0].children[0]
                right_grandchild = expr.children[0].children[0].children[1]

                if left_grandchild.val == "CONST" and right_grandchild.val == "TEMP":

                    if expr.children[1].children[0].val in operators:
                        # print("found you")
                        return left_grandchild.children[0].val + " " + expr.children[1].children[0].val + " " + self.munch_expr(expr.children[1])
                    else:
                        # print("found you")
                        return "x + " + self.munch_constant(expr.children[1])

                elif left_grandchild.val == "TEMP" and right_grandchild.val == "CONST":
                    return "x + " + self.munch_constant(expr.children[1])

        # [MEM[+[TEMP FP, CONST variable]], e]  -- TEMP as the left child

        elif expr.val == "MEM" and expr.children[0].val in operators:             # if MEM and left child is plus
            left_grandchild = expr.children[0].children[0]
            right_grandchild = expr.children[0].children[1]

            if left_grandchild.val == "TEMP" and right_grandchild.val == "CONST":
                return right_grandchild.children[0].val

            elif left_grandchild.val == "CONST" and right_grandchild.val == "TEMP":
                return left_grandchild.children[0].val

    def munch_constant(self, constant):

        return str(constant.children[0].val)

    def loop_check(self, statement):

        global indentation
        # check if the CJUMP is an if or a while statement

        # if the first element if the comparison is an expression

        if statement.children[0].val == "MEM":
            e1 = self.munch_expr(statement.children[0])
        # else it is a constant
        else:
            e1 = self.munch_constant(statement.children[0])

        # operator
        e2 = statement.children[1].val

        if statement.children[2].val == "MEM":
            e3 = self.munch_expr(statement.children[2])

            # else it is a constant
        else:
            e3 = self.munch_constant(statement.children[2])

        if (statement.parent.children[0].val == "LABEL" or statement.parent.children[0].children[1].val == "LABEL") and check == True:

            # add the name of the label to the stack
            if statement.parent.children[0].val == "LABEL":
                stack.insert(0, statement.parent.children[0].children[0].val)

            elif statement.parent.children[0].children[1]:
                stack.insert(0, statement.parent.children[0].children[1].children[0].val)

            # check if the label is start
            if indentation == 0:
                print("while ( " + e1 + " " + comp_op[e2] + " " + e3 + " )")
                indentation += 1
            else:
                print("\t" * indentation + "while ( " + e1 + " " + comp_op[e2] + " " + e3 + " )")
                indentation += 1

        else:
            if indentation == 0:

                print("if ( " + e1 + " " + comp_op[e2] + " " + e3 + " )")
                indentation += 1
            else:
                print("\t" * indentation + "if ( " + e1 + " " + comp_op[e2] + " " + e3 + " )")
                indentation += 1

                # if constant statement.children[2] munch_const
                # else munch_expr statement.children[2]


def print_prefix(root):
    prefix = ""
    for i in range(0, abs(root.level), 1):
        prefix += "="
    return prefix


name = "testdata9.ir"
file = open(name, "r")                  # read in file
ir_tree = Tree()

queue = []
input = []
temp_array = []
root = None
operators = ["+", "-", "/", "*"]
comp_op = {"gt": ">", "GT": ">", "lt": "<", "LT": "<"}
indentation = 0
stack = []
check = False

for line in file:
    if line.endswith("\n"):
        input.append(line[:-1])                       # remove the new line character
    else:
        input.append(line)

for line in input:
    counter = 0
    for char in line:
        if char == "=":                                 # iterate character by character
            counter += 1
    line = line[counter:]

    temp_array.append({"node": line, "level": counter})      # separate the operation and the level

parent = ir_tree.insert(None, temp_array[0]["node"], temp_array[0]["level"])
root = parent

for i in range(1, len(temp_array), 1):

    data = temp_array[i]["node"]
    level = temp_array[i]["level"]
    parent = ir_tree.insert(parent, data, level)

# ir_tree.pre_order(root)

ir_tree.generate_code()
